<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    private $expectedBooks = [
            1 => [
                1 => '1_page1_text1',
                2 => '1_page2_text2'
            ],
            2 => [
                1 => '2_page1_text1',
                2 => '2_page2_text2'
            ]
        ];

    /**
     * Test that the index route returns a rendered response containing the text 'SlimFramework' but not a greeting
     */
    public function testGetHomepageWithoutName()
    {
        $response = $this->runApp('GET', '/books');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($this->expectedBooks, json_decode($response->getBody(), true));
    }


}
