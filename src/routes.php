<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/*return function (App $app) {
    $container = $app->getContainer();

    $app->get('/[{name}]', function (Request $request, Response $response, array $args) use ($container) {
        // Sample log message
        $container->get('logger')->info("Slim-Skeleton '/' route");

        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });
};*/

///
/// New GET
///
return function (App $app) {
    $container = $app->getContainer();

    $app->get('/books', function (Request $request, Response $response, array $args) use ($container) {
        return $response->withJson($container['books']);
    });

    $app->get('/books/[{id}]', function (Request $request, Response $response, array $args) use ($container) {
        $books = $container['books'];
        if (!isset($books[$args['id']])) {
            return $response->withStatus(404);
        }
        $book = $books[$args['id']];

        return $response->withJson($book);
    });

    $app->get('/books/{book_id}/pages/{page_id}', function (Request $request, Response $response, array $args) use ($container) {
        $books = $container['books'];
        if(!isset($books[$args['book_id']][$args['page_id']])){
            return $response->withStatus(404);
        }
        $page = $books[$args['book_id']][$args['page_id']];

        return $response->withJson($page);
    });
};
